customCB = class("customCB")

function customCB:initialize(entity)
    self.exports = {
        variable = "Hello Rootex!"
    }
    self.modelComp = entity.model
    print(entity.model.name)
end

function customCB:enterScene(entity)
end

function customCB:begin(entity)
    orignalMatr = entity.model:getModelResourceFile():getMaterialAt(0)
    overridingMatr = self.modelComp:getMaterialOverride(orignalMatr)
    col = RTX.Color.new(0.0,0.5,0.5,1.0)
    overridingMatr:asCustom():setColor(0, col)
    a = 0.5
    print(overridingMatr:asCustom():setFloat(2, a))
end

function customCB:update(entity, delta)
end

function customCB:destroy(entity)
    print("Everything is permitted")
end

function customCB:enterTrigger(entity, trigger)
end

function customCB:exitTrigger(entity, trigger)
end

return customCB
